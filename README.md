# ECE 5462 - VP1

This repository was created to hold the code for ECE 5462 - Verification Project 1

### Group Members
* Adam Creech
* Ben Bohls
* Derek Kahle
* Jason Merriman
* Luke Brantingham

### Report

The report for this project is hosted on google docs and can be found [here](https://docs.google.com/document/d/1_uAYZzRc2Wv6H712NWML-zlvFstrKnjiHnLGoCmq3-U/edit?usp=sharing)