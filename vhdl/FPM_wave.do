onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /test_bench4/A
add wave -noupdate -radix hexadecimal /test_bench4/B
add wave -noupdate -radix hexadecimal /test_bench4/C
add wave -noupdate -radix hexadecimal /test_bench4/exp_res
add wave -noupdate /test_bench4/latch
add wave -noupdate /test_bench4/drive
add wave -noupdate -radix ascii /test_bench4/aid_sig
add wave -noupdate -radix ascii /test_bench4/bid_sig
add wave -noupdate -radix ascii /test_bench4/resid_sig
add wave -noupdate /test_bench4/err_sig
add wave -noupdate -radix decimal /test_bench4/applytest/num_errors
add wave -noupdate /test_bench4/nm
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {32950 ns} {33950 ns}


